package by.gsu.igi.lectures.lecture08;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

/**
 * @author Evgeniy Myslovets
 */
public class ReadingDemo {

    public static void main(String[] args) throws IOException {
        copyBytes(new FileInputStream("README.md"), System.out);
        copyBytes(new URL("http://teach.myslovets.by/").openStream(), System.out);
        copyBytes(new ByteArrayInputStream("Hello World".getBytes()), System.out);
        copyBytes(System.in, System.out);
    }

    private static void copyBytes(InputStream input, OutputStream output) throws IOException {
        input = new BufferedInputStream(input);
        try {
            int b;
            while ((b = input.read()) != -1) {
                output.write((byte) b);
            }
        } finally {
            input.close();
            output.flush();
        }
    }
}
