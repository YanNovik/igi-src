package by.gsu.igi.lectures.lecture06;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Author: Evgeniy Myslovets
 */
public class GenericsAdditionalDemo {

    public static void printCollection(Collection<?> c) {
        for (Object o : c) {
            System.out.println(o);
        }
    }

    public static void print(Collection<? extends Number> c) {
        for (Object o : c) {
            System.out.println(o);
        }
    }

    public static void addPI(Collection<? super Double> c) {
        c.add(Math.PI);
    }

    public static void main(String[] args) {
        String[] array = {"Hello"};
        String hello = array[0];

        List<String> list = new ArrayList<String>();
        list.add("Hello");
        list.add("World");

        printCollection(list);

//        print(list);
//        print(new ArrayList<Object>());
        print(new ArrayList<Number>());
        print(new ArrayList<Double>());
        print(new ArrayList<Integer>());

//        addPI(list);
        addPI(new ArrayList<Object>());
        addPI(new ArrayList<Number>());
        addPI(new ArrayList<Double>());
//		addPI(new ArrayList<Integer>());
    }
}
