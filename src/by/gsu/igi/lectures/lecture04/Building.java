package by.gsu.igi.lectures.lecture04;

/**
 * Created by myslovets on 12.09.2016.
 */
public class Building {

    private int constructionYear;

    public Building(int constructionYear) {
        this.constructionYear = constructionYear;
    }

/*
    public Building() {
        this.constructionYear = 1950;
    }
*/

    public int getConstructionYear() {
        return constructionYear;
    }

    @Override
    public String toString() {
        return "Building{" +
                "constructionYear=" + constructionYear +
                '}';
    }
}
