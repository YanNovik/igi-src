package by.gsu.igi.lectures.lecture04;

/**
 * Created by myslovets on 12.09.2016.
 */
public class Cat extends Animal {

    @Override
    public void makeSound() {
        System.out.println("Mauu");
    }

    @Override
    public void touch() {
        System.out.println("Mrrrr");
    }
}
