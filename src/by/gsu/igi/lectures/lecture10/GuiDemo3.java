package by.gsu.igi.lectures.lecture10;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created: 21.11.2016
 *
 * @author Evgeniy Myslovets
 */
public class GuiDemo3 {

    public static void main(String[] args) {
        GuiDemo3 gui = new GuiDemo3();
        gui.go();
    }

    private void go() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        MySimpleDrawPanel drawPanel = new MySimpleDrawPanel();

        frame.getContentPane().add(drawPanel);
        frame.setSize(300, 300);
        frame.setVisible(true);
    }
}
