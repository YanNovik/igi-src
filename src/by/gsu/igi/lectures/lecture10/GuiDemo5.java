package by.gsu.igi.lectures.lecture10;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created: 21.11.2016
 *
 * @author Evgeniy Myslovets
 */
public class GuiDemo5 {

    private JFrame frame;
    private JLabel label;
    private JButton colorButton;

    public static void main(String[] args) {
        GuiDemo5 gui = new GuiDemo5();
        gui.go();
    }

    private void go() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        colorButton = new JButton("Изменить цвет");
        colorButton.addActionListener(new ColorListener());

        JButton labelButton = new JButton("Изменить подпись");
        labelButton.addActionListener(new LabelListener());

        label = new JLabel("Я - подпись");

        MyDrawPanel drawPanel = new MyDrawPanel();

        frame.getContentPane().add(BorderLayout.SOUTH, colorButton);
        frame.getContentPane().add(BorderLayout.CENTER, drawPanel);
        frame.getContentPane().add(BorderLayout.EAST, labelButton);
        frame.getContentPane().add(BorderLayout.WEST, label);
        frame.setSize(500, 300);
        frame.setVisible(true);
    }

    private class ColorListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            frame.repaint();
        }
    }

    private class LabelListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            label.setText("Упс!");
        }
    }

/*
    @Override
    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == colorButton) {
            frame.repaint();
        } else {
            label.setText("Упс!");
        }
    }
*/

/*
    @Override
    public void actionPerformed(ActionEvent e) {
        label.setText("Упс!");
    }
*/
}
