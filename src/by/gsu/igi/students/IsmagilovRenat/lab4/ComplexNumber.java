package by.gsu.igi.students.IsmagilovRenat.lab4;

/**
 * Created by RISMAGILOV on 24.11.16.
 */
public class ComplexNumber {
    private double real;
    private double imaginaries;
    private double module;
    private double corner;

    public ComplexNumber(double real, double imaginaries) {
        this.real = real;
        this.imaginaries = imaginaries;
        module = Math.sqrt(real * real + imaginaries * imaginaries);
        fixModuleAndCorner(real, imaginaries);
    }

    public ComplexNumber() {
        this(0, 0);
    }

    public double getReal() {
        return real;
    }

    public double getImaginaries() {
        return imaginaries;
    }

    public ComplexNumber add(ComplexNumber complexNumber) {
        return new ComplexNumber(real + complexNumber.getReal(), imaginaries + complexNumber.getImaginaries());
    }

    public ComplexNumber substract(ComplexNumber complexNumber) {
        return new ComplexNumber(real - complexNumber.getReal(), imaginaries - complexNumber.getImaginaries());
    }

    public ComplexNumber multiply(ComplexNumber complexNumber) {
        double real = this.real * complexNumber.getReal() - this.imaginaries * complexNumber.getImaginaries();
        double imaginaries = real * complexNumber.getImaginaries() + this.imaginaries * complexNumber.getReal();
        return new ComplexNumber(real, imaginaries);
    }

    public ComplexNumber divide(ComplexNumber q) {
        double real = (this.real * q.getReal() + this.imaginaries * q.getImaginaries()) / (this.real * this.real + q.getReal() * q.getReal());
        double imaginaries = (this.imaginaries * q.getReal() - this.real * q.getImaginaries()) / (this.real * this.real + q.getReal() * q.getReal());
        return new ComplexNumber(real, imaginaries);
    }

    @Override
    public String toString() {
        String s = showAlgebraic() + "\n" + showTrigonometric();
        return s;
    }

    public String showAlgebraic() {
        return "z = " + real + " + i*" + imaginaries;
    }

    public String showTrigonometric() {
        return "z = " + module + "*(cos(" + corner + ") + sin(" + corner + "))";
    }

    private void fixModuleAndCorner(double real, double imaginaries) {
        if (real != 0) {
            corner = Math.atan(imaginaries / real);
        } else {
            if (imaginaries != 0) {
                corner = Math.signum(real) * (Math.PI / 2);
            } else {
                corner = 0;
            }

        }
    }
}