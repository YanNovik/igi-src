package by.gsu.igi.students.YanNovik.lab5;

public interface Speakable {
    void speak();
}
