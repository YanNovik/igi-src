package by.gsu.igi.students.VladShapetska.lab4;

public class Runner {
    public static void main(String[] args) {
        ComplexNumber z = new ComplexNumber(1, 1);
        ComplexNumber q = new ComplexNumber(1, 1);
        System.out.println("sum:");
        System.out.println(z.add(q));
        System.out.println("difference:");
        System.out.println(z.substract(q));
        System.out.println("multiplication:");
        System.out.println(z.multiply(q));
        System.out.println("division:");
        System.out.println(z.divide(q));
    }

}
