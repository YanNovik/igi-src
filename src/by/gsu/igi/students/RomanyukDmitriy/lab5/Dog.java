package by.gsu.igi.students.RomanyukDmitriy.lab5;

/**
 * Created by Romanyuk Dmitriy.
 */
public class Dog implements Speakable {
    @Override
    public void speak() {
        System.out.println("Vuf-vuf");
    }
}
